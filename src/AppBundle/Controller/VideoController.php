<?php

declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Form\VideoType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VideoController extends Controller
{
    const VIDEO_LIST_ROUTE_NAME = 'video.list';

    /**
     * Get a list of video's for the logged in user
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @return Response
     */
    public function getListAction(): Response
    {
        /** @var User $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        $videos = $this->getDoctrine()
            ->getRepository(Video::class)
            ->findBy(['user' => $user->getId()]);

        return $this->render(
            'AppBundle:Video:list.html.twig', [
                'videos' => $videos
            ]
        );
    }

    /**
     * Add a new video with data for the logged in user
     *
     * @param Request $request
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws FileException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request): Response
    {
        /** @var User $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getEntityManager();

        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded video file
            /** @var UploadedFile $file */
            $file = $video->getFile();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('videos_directory'),
                $fileName
            );

            $video->setName($request->get('name'));
            $video->setDescription($request->get('description'));
            $video->setFile($fileName);
            $video->setUser($user);

            $em->persist($video);
            $em->flush();

            $this->addFlash('notice', 'Video is added!');

            return new RedirectResponse($this->generateUrl(self::VIDEO_LIST_ROUTE_NAME));
        }

        return $this->render('AppBundle:Video:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Delete a specific video
     *
     * @param Request $request
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws NotFoundHttpException
     * @return RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getEntityManager();

        /** @var User $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        $video = $this->getDoctrine()
            ->getRepository(Video::class)
            ->findOneBy(['id' => $id, 'user' => $user]);

        if (!$video) {
            throw $this->createNotFoundException('Delete is not possible: '. $id);
        }

        $em->remove($video);
        $em->flush();

        $this->addFlash('notice', 'Video is deleted: '.$id);

        return new RedirectResponse($this->generateUrl(self::VIDEO_LIST_ROUTE_NAME));
    }
}
