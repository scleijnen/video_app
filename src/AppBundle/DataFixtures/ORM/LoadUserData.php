<?php

declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected static $userData = [
        'user.test1' => [
            'userName'      => 'testuser1',
            'email'         => 'testuser1@domain.com',
            'plainPassword' => 'testuser1!',
            'enabled'       => true,
            'roles'         => ['ROLE_USER'],
        ],
        'user.test2' => [
            'userName'      => 'testuser2',
            'email'         => 'testuser2@domain.com',
            'plainPassword' => 'testuser2!',
            'enabled'       => true,
            'roles'         => ['ROLE_USER'],
        ],
    ];

    /**
     * @param ObjectManager $manager
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');

        foreach (static::$userData as $ref => $data) {
            $user = $userManager->createUser();
            $user->setUsername($data['userName']);
            $user->setEmail($data['email']);
            $user->setPlainPassword($data['plainPassword']);
            $user->setEnabled($data['enabled']);
            $user->setRoles($data['roles']);

            $userManager->updateUser($user);
        }
    }
}

