<?php

declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Video;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;

class LoadVideoData implements FixtureInterface
{
    protected static $videoData = [
        'video.test1' => [
            'name'        => 'Test Video 1',
            'description' => 'This is a test video for demonstration 1',
            'file'        => 'TestVideo1.mp4',
            'user'        => 1,
        ],
        'video.test2' => [
            'name'        => 'Test Video 2',
            'description' => 'This is a test video for demonstration 2',
            'file'        => 'TestVideo2.mp4',
            'user'        => 2,
        ],
        'video.test3' => [
            'name'        => 'Test Video 3',
            'description' => 'This is a test video for demonstration 3',
            'file'        => 'TestVideo3.mp4',
            'user'        => 1,
        ],
        'video.test4' => [
            'name'        => 'Test Video 4',
            'description' => 'This is a test video for demonstration 4',
            'file'        => 'TestVideo4.mp4',
            'user'        => 2,
        ],
        'video.test5' => [
            'name'        => 'Test Video 5',
            'description' => 'This is a test video for demonstration 5',
            'file'        => 'TestVideo5.mp4',
            'user'        => 1,
        ],
        'video.test6' => [
            'name'        => 'Test Video 6',
            'description' => 'This is a test video for demonstration 6',
            'file'        => 'TestVideo6.mp4',
            'user'        => 2,
        ],
    ];

    /**
     * @param ObjectManager $manager
     * @throws ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        foreach (static::$videoData as $ref => $data) {
            /* @var Video $video */
            $video = new Video();

            /* @var User $user */
            $user = $manager->getRepository(User::class)->findOneBy(['id' => $data['user']]);

            $video->setName($data['name']);
            $video->setDescription($data['description']);
            $video->setFile($data['file']);
            $video->setUser($user);

            $manager->persist($video);
            $manager->flush();
        }
    }
}

