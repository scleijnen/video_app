<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\NotNull;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('file', FileType::class, array('label' => 'Video (mp4 file)'))
            ->add('user', EntityType::class, [
                'class' => User::class,
                'constraints' => [
                    new NotNull()
                ]
            ])
        ;

        $builder->add('save', ButtonType::class, array(
            'attr' => array('class' => 'save'),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Video::class,
        ));
    }
}
